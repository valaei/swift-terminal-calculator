//
//  Stack.swift
//  calc
//
//  Created by Arian Valaei on 17/3/21.
//  Copyright © 2021 UTS. All rights reserved.
//

import Foundation

struct Stack {
    private var stackArray: [String] = []
    
    mutating func push(_ element: String) {
        stackArray.append(element);
    }
    
    mutating func pop() -> String? {
        return stackArray.popLast();
    }
    
    func peek() -> String? {
        return stackArray.last;
    }
    
    func getSize() -> Int {
        return stackArray.count;
    }
    
    func getIndex(index: Int) -> String {
        return stackArray[index];
    }
}
