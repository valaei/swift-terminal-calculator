//
//  Calculator.swift
//  calc
//
//  Created by Jacktator on 31/3/20.
//  Copyright © 2020 UTS. All rights reserved.
//

import Foundation

class Calculator {
    
    var inputStack: Stack;
    var operationStack: Stack;
    var resultStack: Stack;
    
    let validOperations: [String] = ["+", "-", "x", "/", "%"];
    
    init() {
        self.inputStack = Stack();
        self.operationStack = Stack();
        self.resultStack = Stack();

    }
    
    func add(no1: Int, no2: Int) -> Int {
        return no1 + no2;
    }
    
    // Perform Substraction
    func sub(no1: Int, no2: Int) -> Int {
        return no1 - no2;
    }
    
    // Perform Multiplication
    func mult(no1: Int, no2: Int) -> Int {
        return no1 * no2;
    }
    
    // Perform Division
    func div(no1: Int, no2: Int) throws -> Int {
        if (no2 == 0) {
            throw CalculateError.DivisionByZero;
        }
        return no1 / no2;
    }
    
    // Perform Modulus
    func mod(no1: Int, no2: Int) -> Int {
        return no1 % no2;
    }
    
    // Perform no1 op no2
    func calcSingleArithmetic(no1: Int, op: String , no2: Int) throws -> Int {
        if (no1 < Int.min || no1 > Int.max || no2 < Int.min || no2 > Int.max ) {
            throw CalculateError.IntegerOutOfRange;
        }
        var result: Int = 0;
        switch op {
        case "+":
            result = add(no1: no1, no2: no2);
            break;
        case "-":
            result = sub(no1: no1, no2: no2);
            break;
        case "x":
            result = mult(no1: no1, no2: no2);
            break;
        case "/":
            do {
                try result = div(no1: no1, no2: no2);
            } catch CalculateError.DivisionByZero {
                throw CalculateError.DivisionByZero;
            }
            break;
        case "%":
            result =  mod(no1: no1, no2: no2);
            break;
        default:
            break;
        }
        return result;
    }
    
    // Check Operation is Valid
    func isInputValid(input: [String]) -> Bool {
        let size = input.count;
        if (size % 2 == 0) {
            return false;
        }
        for index in (0..<size).reversed() {
            let element = input[index];
            if (index % 2 == 0) {
                let num = Int(element);
                if (num == nil) {
                    return false;
                }
            } else {
                if (!validOperations.contains(element)) {
                    return false;
                }
            }
            self.inputStack.push(element);
        }
        return true;
    }
    
    func hasGreaterOrEqualPrecedence(op1: String, op2: String) -> Bool {
        let operationList1 = ["x", "/", "%"];
        let operationList2 = ["+", "-"];
        if(op1 == "" || op2 == "") {
            return false;
        }
        if (operationList1.contains(op2) && operationList2.contains(op1)) {
            return false;
        }
        return true;
    }
    
    // Check Validation
    // Calculation Using Shunting-Yard Algorithm
    func calculate(args: [String]) -> String {
        if(!isInputValid(input: args)) {
            fatalError("Invalid Operation, Check Your Input!");
        }
        while (inputStack.getSize() != 0) {
            if (inputStack.getSize() % 2 == 1) { // Element is a Number
                resultStack.push(inputStack.pop()!);
            } else { // Element is an Operator
                let operation = inputStack.pop();
                while (operationStack.getSize() != 0 &&
                       hasGreaterOrEqualPrecedence(op1: operationStack.peek() ?? "", op2: operation ?? "")) {
                        do {
                            let no2 = Int(resultStack.pop()!);
                            let no1 = Int(resultStack.pop()!);
                            let op = operationStack.pop()!;
                            let result = try calcSingleArithmetic(no1: no1!, op: op, no2: no2!);
                            resultStack.push(String(result));
                            
                            
                        } catch CalculateError.DivisionByZero {
                            fatalError("Division By Zero Is Not Permitted! ");

                        } catch CalculateError.IntegerOutOfRange {
                            fatalError("Integer Out OF Range!");

                        } catch {
                            fatalError("Operation Not Permitted!");
                        }
                    
                }
                operationStack.push(operation!);
            }
        }
        while (operationStack.getSize() != 0) {
            do {
                let no2 = Int(resultStack.pop()!);
                let no1 = Int(resultStack.pop()!);
                let op = operationStack.pop()!;
                let result = try calcSingleArithmetic(no1: no1!, op: op, no2: no2!);
                resultStack.push(String(result));
                
                
            } catch CalculateError.DivisionByZero {
                fatalError("Division By Zero Is Not Permitted! ");

            } catch CalculateError.IntegerOutOfRange {
                fatalError("Integer Out OF Range!");

            } catch {
                fatalError("Operation Not Permitted!");
            }
        }
        
        return String(Int(resultStack.pop()!)!);
    
    }
    
    enum CalculateError: Error {
        case DivisionByZero
        case IntegerOutOfRange
    }
}
